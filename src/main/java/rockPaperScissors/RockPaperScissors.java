package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        while (true) {
            System.out.println(" Let's play round " + roundCounter);
            String player_choice;
            while (true) {
                String player_input = readInput("Your choice (Rock/Paper/Scissors)?");
                if (player_input.equals("rock") || player_input.equals("paper") || player_input.equals("scissors")) {
                    player_choice=player_input;
                    break;
                }
                System.out.println("I do not understand " + player_input + ". Could you try again?");
            }
            String computer_choice = rpsChoices.get(1);
            if (player_choice.equals(computer_choice)) {
                System.out.println("Human chose " + player_choice + ", computer chose " + computer_choice + ". It's a tie!");
            }
            else if (player_choice.equals("scissors")){
                System.out.println("Human chose scissors, computer chose " + computer_choice + ". Human wins!");
                humanScore ++;
            }
            else if (player_choice.equals("rock")){
                System.out.println("Human chose rock, computer chose " + computer_choice + ". Computer wins!");
                computerScore ++;
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            String y_or_n = readInput("Do you wish to continue playing? (y/n)?");
            if (y_or_n.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter +=1;
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
